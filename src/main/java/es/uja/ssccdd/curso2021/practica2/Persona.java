/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica2;

import static es.uja.ssccdd.curso2021.practica2.Utils.NUM_OPERACIONES;
import static es.uja.ssccdd.curso2021.practica2.Utils.NUM_PLANTAS;
import static es.uja.ssccdd.curso2021.practica2.Utils.TIEMPO_MAX_PERSONA_PLANTA;
import static es.uja.ssccdd.curso2021.practica2.Utils.SUBIDA;
import static es.uja.ssccdd.curso2021.practica2.Utils.BAJADA;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Persona implements Runnable {

    private final int iD;
    private final Monitor monitor;

    public Persona(int iD, Monitor monitor) {
        this.iD = iD;
        this.monitor = monitor;
    }

    @Override
    public void run() {

        try {

            System.out.println("Persona " + iD + " - Iniciada ejecución");

            int pasos = 0;
            int plantaActual = 0;
            int plantaDestino = ThreadLocalRandom.current().nextInt(NUM_PLANTAS - 1) + 1;
            int direccion = SUBIDA;

            while (pasos <= NUM_OPERACIONES && (plantaActual != 0 || pasos == 0)) {

                System.out.println("Persona " + iD + " - Planta " + plantaActual + " -> " + plantaDestino);

                int ascensor = monitor.esperarAscensor(plantaActual, direccion);
                
                System.out.println("Persona " + iD + " - Subiendo al ascensor " + ascensor);
                
                monitor.subidoAscensor(plantaActual, plantaDestino);

                int tiempoEspera = ThreadLocalRandom.current().nextInt(TIEMPO_MAX_PERSONA_PLANTA);
                System.out.println("Persona " + iD + " - Llegada a planta " + plantaDestino);

                TimeUnit.SECONDS.sleep(ThreadLocalRandom.current().nextInt(TIEMPO_MAX_PERSONA_PLANTA));

                plantaActual = plantaDestino;
                pasos++;

                plantaDestino = pasos == NUM_OPERACIONES ? 0 : generarPlantaDestino(plantaActual);

                direccion = plantaDestino > plantaActual ? SUBIDA : BAJADA;

            }

            System.out.println("Persona " + iD + " - Saliendo del edificio");

        } catch (InterruptedException ex) {
            Logger.getLogger(Persona.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private int generarPlantaDestino(int plantaActual) {

        int plantaDestino;

        do {
            plantaDestino = ThreadLocalRandom.current().nextInt(NUM_PLANTAS);
        } while (plantaDestino == plantaActual);

        return plantaDestino;

    }

}
