/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica2;

import static es.uja.ssccdd.curso2021.practica2.Utils.CAPACIDAD_ASCENSOR;
import static es.uja.ssccdd.curso2021.practica2.Utils.NUM_ASCENSORES;
import static es.uja.ssccdd.curso2021.practica2.Utils.NUM_PLANTAS;
import static es.uja.ssccdd.curso2021.practica2.Utils.BAJADA;
import static es.uja.ssccdd.curso2021.practica2.Utils.SUBIDA;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Monitor {

    private Ascensor ascensorALaEspera;
    private int direccionAscensor;
    private int iDAscensor;
    private final ArrayList<ArrayList<Integer>> contEsperaPlanta;
    private final ArrayList<ArrayList<Integer>> contEsperaAscensor;
    private int contAscensoresALaEspera;

    private final Lock lockMonitor;
    private final ArrayList<ArrayList<Condition>> esperaPlanta;
    private final ArrayList<ArrayList<Condition>> esperaAscensor;
    private final Condition ascensorEnPlanta;
    private final Condition ascensoresALaEspera;

    public Monitor() {

        this.contEsperaPlanta = new ArrayList<>();
        this.contEsperaAscensor = new ArrayList<>();

        this.lockMonitor = new ReentrantLock();
        this.esperaPlanta = new ArrayList<>();
        this.esperaAscensor = new ArrayList<>();
        this.ascensorEnPlanta = lockMonitor.newCondition();
        this.ascensoresALaEspera = lockMonitor.newCondition();
        this.contAscensoresALaEspera = 0;

        this.esperaPlanta.add(new ArrayList<>());//Bajada
        this.esperaPlanta.add(new ArrayList<>());//Subida

        this.contEsperaPlanta.add(new ArrayList<>());//Bajada
        this.contEsperaPlanta.add(new ArrayList<>());//Subida

        for (int i = 0; i < NUM_ASCENSORES; i++) {
            esperaAscensor.add(new ArrayList<>());
            contEsperaAscensor.add(new ArrayList<>());
        }

        for (int i = 0; i < NUM_PLANTAS; i++) {
            this.esperaPlanta.get(BAJADA).add(lockMonitor.newCondition());
            this.contEsperaPlanta.get(BAJADA).add(0);

            this.esperaPlanta.get(SUBIDA).add(lockMonitor.newCondition());
            this.contEsperaPlanta.get(SUBIDA).add(0);

            for (int j = 0; j < NUM_ASCENSORES; j++) {
                esperaAscensor.get(j).add(lockMonitor.newCondition());
                contEsperaAscensor.get(j).add(0);
            }
        }

    }

    public int esperarAscensor(int planta, int direccion) throws InterruptedException {
        lockMonitor.lock();

        contEsperaPlanta.get(direccion).set(planta, contEsperaPlanta.get(direccion).get(planta) + 1);
        esperaPlanta.get(direccion).get(planta).await();

        int ascensorMontado = iDAscensor;
        lockMonitor.unlock();
        return ascensorMontado;
    }

    public void ascensorEnPlanta(Ascensor ascensor, int planta, int direccion, int iD) throws InterruptedException {
        lockMonitor.lock();

        //Para evitar la entrada de varios ascensores
        if (contAscensoresALaEspera > 0) {
            ascensoresALaEspera.await();
        }

        contAscensoresALaEspera++;
        ascensorALaEspera = ascensor;
        direccionAscensor = direccion;
        iDAscensor = iD;

        //Personas que salen del ascensor
        if (contEsperaAscensor.get(iD).get(planta) > 0) {
            contEsperaAscensor.get(iD).set(planta, contEsperaAscensor.get(iD).get(planta) - 1);
            esperaAscensor.get(iD).get(planta).signal();
            ascensorEnPlanta.await();

        } else {//Personas que entran
            if (contEsperaPlanta.get(direccion).get(planta) > 0 && ascensorALaEspera.getOcupacion() < CAPACIDAD_ASCENSOR) {
                contEsperaPlanta.get(direccion).set(planta, contEsperaPlanta.get(direccion).get(planta) - 1);
                esperaPlanta.get(direccion).get(planta).signal();
                ascensorEnPlanta.await();
            }
        }

        contAscensoresALaEspera--;
        ascensoresALaEspera.signal();

        lockMonitor.unlock();
    }

    public void subidoAscensor(int plantaActual, int plantaDestino) throws InterruptedException {
        lockMonitor.lock();

        ascensorALaEspera.aumentarOcupacion();

        //Personas que entran
        if (contEsperaPlanta.get(direccionAscensor).get(plantaActual) > 0 && ascensorALaEspera.getOcupacion() < CAPACIDAD_ASCENSOR) {
            contEsperaPlanta.get(direccionAscensor).set(plantaActual, contEsperaPlanta.get(direccionAscensor).get(plantaActual) - 1);
            esperaPlanta.get(direccionAscensor).get(plantaActual).signal();

        } else {
            ascensorEnPlanta.signal();
        }

        //Espera en el ascensor
        contEsperaAscensor.get(iDAscensor).set(plantaDestino, contEsperaAscensor.get(iDAscensor).get(plantaDestino) + 1);
        esperaAscensor.get(iDAscensor).get(plantaDestino).await();

        ascensorALaEspera.decrementarOcupacion();

        //Personas que salen del ascensor
        if (contEsperaAscensor.get(iDAscensor).get(plantaDestino) > 0) {
            contEsperaAscensor.get(iDAscensor).set(plantaDestino, contEsperaAscensor.get(iDAscensor).get(plantaDestino) - 1);
            esperaAscensor.get(iDAscensor).get(plantaDestino).signal();

        } else {
            //Personas que entran
            if (contEsperaPlanta.get(direccionAscensor).get(plantaDestino) > 0 && ascensorALaEspera.getOcupacion() < CAPACIDAD_ASCENSOR) {
                contEsperaPlanta.get(direccionAscensor).set(plantaDestino, contEsperaPlanta.get(direccionAscensor).get(plantaDestino) - 1);
                esperaPlanta.get(direccionAscensor).get(plantaDestino).signal();

            } else {
                ascensorEnPlanta.signal();
            }
        }
        
        lockMonitor.unlock();
    }
}
