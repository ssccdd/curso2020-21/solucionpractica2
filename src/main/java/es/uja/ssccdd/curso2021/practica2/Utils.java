/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica2;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    // Constantes 
    public static final int BAJO = 0;
    public static final int SUBIDA = 0;
    public static final int BAJADA = 1;
    public static final int NUM_ASCENSORES = 3;
    public static final int NUM_PLANTAS = 7;
    public static final int NUM_PERSONAS = 15;
    public static final int NUM_OPERACIONES = 5;
    public static final int CAPACIDAD_ASCENSOR = 3;
    public static final int TIEMPO_MAX_PERSONA_PLANTA = 30; //En segundos
    public static final int TIEMPO_MAX_ASCENSOR_ENTRE_PLANTAS = 3; //En segundos
    public static final int TIEMPO_MAX_GENERACIÓN_PERSONAS = 10; //En segundos

    

}
