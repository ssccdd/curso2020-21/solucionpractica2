/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica2;

import static es.uja.ssccdd.curso2021.practica2.Utils.SUBIDA;
import static es.uja.ssccdd.curso2021.practica2.Utils.BAJADA;
import static es.uja.ssccdd.curso2021.practica2.Utils.NUM_PLANTAS;
import static es.uja.ssccdd.curso2021.practica2.Utils.TIEMPO_MAX_ASCENSOR_ENTRE_PLANTAS;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ascensor implements Runnable {

    private final int iD;
    private final Monitor monitor;
    private int ocupacion;
    private int direccion;
    private int plantaActual;

    public Ascensor(int iD, Monitor monitor) {
        this.iD = iD;
        this.monitor = monitor;
        ocupacion = 0;
    }

    @Override
    public void run() {
        try {

            direccion = SUBIDA;
            int plantaActual = 0;

            System.out.println("Ascensor " + iD + " - Iniciada ejecución");

            while (true) {
                System.out.println("Ascensor " + iD + " - En planta " + plantaActual);
                monitor.ascensorEnPlanta(this, plantaActual, direccion, iD);

                System.out.println("Ascensor " + iD + " - " + (direccion == BAJADA ? "Bajando" : "Subiendo"));
                TimeUnit.SECONDS.sleep(ThreadLocalRandom.current().nextInt(TIEMPO_MAX_ASCENSOR_ENTRE_PLANTAS));

                plantaActual = direccion == SUBIDA ? plantaActual + 1 : plantaActual - 1;
                direccion = plantaActual == NUM_PLANTAS - 1 ? BAJADA : (plantaActual == 0 ? SUBIDA : direccion);

            }

        } catch (InterruptedException ex) {
            System.out.println("Ascensor " + iD + " - Terminando");
        }

    }

    public void aumentarOcupacion() {
        ocupacion++;
    }

    public void decrementarOcupacion() {
        ocupacion--;
    }

    public int getOcupacion() {
        return ocupacion;
    }

}
