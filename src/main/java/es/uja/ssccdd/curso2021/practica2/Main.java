/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica2;

import static es.uja.ssccdd.curso2021.practica2.Utils.TIEMPO_MAX_GENERACIÓN_PERSONAS;
import static es.uja.ssccdd.curso2021.practica2.Utils.NUM_PERSONAS;
import static es.uja.ssccdd.curso2021.practica2.Utils.NUM_ASCENSORES;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executorPersonas = (ExecutorService) Executors.newCachedThreadPool();
        ExecutorService executorAscensores = (ExecutorService) Executors.newCachedThreadPool();
        Monitor monitor = new Monitor();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando ascensores");
        for (int i = 0; i < NUM_ASCENSORES; i++) {
            executorAscensores.execute(new Ascensor(i, monitor));
        }

        System.out.println("HILO-Principal Generando personas");
        for (int i = 0; i < NUM_PERSONAS; i++) {
            executorPersonas.execute(new Persona(i, monitor));
            if (i < NUM_PERSONAS - 1) {
                TimeUnit.SECONDS.sleep(ThreadLocalRandom.current().nextInt(TIEMPO_MAX_GENERACIÓN_PERSONAS));
            }
        }

        System.out.println("HILO-Principal Espera a los centros");
        executorPersonas.shutdown();

        try {
            executorPersonas.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Detiene a los ascensores");
        executorAscensores.shutdownNow();

        try {
            executorAscensores.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

    }

}
